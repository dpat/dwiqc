FROM diannepat/fsl6-core-nogpu

# remove must happen in same layer as install to be effective
RUN apt-get update && \
apt-get install -y python3 \
&& rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY run.py /run.py
COPY version /version
# Make the dwiqc directory and put it in the path
RUN mkdir /dwiqc
ENV DWIQC_HOME=/dwiqc
ENV PATH=${DWIQC_HOME}:${PATH}

WORKDIR ${DWIQC_HOME}

COPY fsl_anat_alt group_qc.sh optiBET.sh qc.sh dwiqc_functions.sh dir_exist.py file_counter.py ${DWIQC_HOME}/

ENTRYPOINT ["/run.py"]
