#!/bin/bash

#==============================================================================

: <<COMMENTBLOCK

Function: QC
Purpose: Prepare dwi data and fieldmaps by registering, creating masks, distortion correction and eddy, followed by eddy_quad. Runs one subject.
Input: participant_label(s)
Output: output directory containing anat, fmap and dwi and under dwi
        data.qc with the quality assessment for that participant
        additional values in the text file quad_folders.txt=
COMMENTBLOCK

QC ()
{
# run one subject
docker run -ti --rm -v ${PWD}:/bids_dataset -v ${PWD}/derivatives/fsl_dwi_proc:/outputs diannepat/dwiqc:latest /bids_dataset /outputs participant --participant_label ${subjectnum} --skip_bids_validator

}

#==============================================================================

: <<COMMENTBLOCK

Function: QC_All
Purpose: Prepare dwi data and fieldmaps by registering, creating masks, distortion correction and eddy, followed by eddy_quad. Runs all subjects.
Input: participant_label(s)
Output: output directory containing anat, fmap and dwi and under dwi
        data.qc with the quality assessment for that participant
        additional values in the text file quad_folders.txt
COMMENTBLOCK

QC_All ()
{
# run them all
docker run -ti --rm -v ${PWD}:/bids_dataset -v ${PWD}/derivatives/fsl_dwi_proc:/outputs diannepat/dwiqc:latest /bids_dataset /outputs participant --skip_bids_validator
}

#==============================================================================

: <<COMMENTBLOCK

Function: QC_Group
Purpose:  Run group level quality reports and update individual reports
Input:    Requires that the output directory contain quad_folders.txt
          (which contains a list of relative paths to participant data.qc directories)
Output:   The squad folder in the output directory and updates to individual
          participant data.qc folders.


COMMENTBLOCK

QC_Group ()
{

docker run -ti --rm -v ${PWD}:/bids_dataset -v ${PWD}/derivatives/fsl_dwi_proc:/outputs diannepat/dwiqc:latest /bids_dataset /outputs group --skip_bids_validator

}

#==============================================================================

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
echo "You must always be in your bids directory"
echo "Run for selected subjects (e.g., 327 is sub-327 etc.):"
echo "Example: dwiqc_wrap.sh QC 327 328"
echo "Run for all available subjects:"
echo "example: dwiqc_wrap.sh QC_All"
echo "============================"
echo "Run group level reports (depends on individual reports)"
echo "example: dwiqc_wrap.sh QC_Group"

echo
exit 1
}

#==============================================================================
: <<COMMENTBLOCK
Parse the input arguments to determine which command to run.
COMMENTBLOCK

###################
# If no arguments are given, display the Help Message.
# Otherwise the first argument is always func.
if [ $# -lt 1 ]; then
  HelpMessage
else
  func=$1
fi
###############################################
##################QC Functions#################

# If there is more than one argument and the func=QC
# then shift and all the rest of the arguments are subjectnums
if [ $# -gt 1 ] && [ ${func} = QC ]; then
func=$1
shift
subjectnum=$*
QC ${subjectnum}
fi

# If there is one argument and func=QC_All,
# then run QC for all subjects in the directory
if [ $# -eq 1 ] && [ ${func} = QC_All ]; then
QC_All
fi


# If there is one argument and func=QC_Group,
# then run QC_Group
if [ $# -eq 1 ] && [ ${func} = QC_Group ]; then
QC_Group
fi

#==============================================================================
