#!/bin/bash

#==============================================================================

: <<COMMENTBLOCK

Function:   QC_group
Purpose:    Run eddy squad
Input:      1) A subdirectory ${output_dir}/${subj}/dwi/data.qc containing quality
            assessment for the participant level dwi image
            2) A text file pointing to each data.qc directory: ${output_dir}/quad_folders.txt
Output:     The results of running eddy_squad in ${output_dir}/squad

COMMENTBLOCK

QC_group ()
{
echo "Running eddy_squad: See ${output_dir}/squad for results"

if [ -d ${output_dir}/squad ]; then
  rm -fr ${output_dir}/squad
fi

cd ${output_dir}
eddy_squad quad_folders.txt -u  

}

#==============================================================================

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
echo
echo "Assuming a bids compliant data directory,"
echo "You should be in your bids_dir."
echo "You need the data.qc directory for each subject"
echo "and the list of those directories in your output dir: quad_folders.txt"
echo "group qc generates reports in your output_dir under squad"
echo
exit 1
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Main
Purpose:    Run group quality control metrics
Input:      The results of running eddy_quad from QC_participant in qc.sh:
            1) a data.qc subfolder in each subject output dwi folder
            2) a text file "quad_folders.txt" in the output folder.
Output:     Group dwi quality assessment report and updated individual
            reports in ${output_dir}/squad

COMMENTBLOCK

Main ()
{

echo "group quality control metrics calculating"
QC_group

}

#==============================================================================
: <<COMMENTBLOCK
See Main
COMMENTBLOCK

# We pass 1 arguments from run.py: the subjectnum, bids_dir and output_dir

output_dir=$1
###################

echo "output_dir = ${output_dir}"

if [ $# -lt 1 ]
    then
        HelpMessage
        exit 1
fi

Main ${output_dir}
#==============================================================================
