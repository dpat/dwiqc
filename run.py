#!/usr/bin/env python3
import argparse
import os
import subprocess
from glob import glob

__version__ = open(os.path.join(os.path.dirname(os.path.realpath(__file__)),
                                'version')).read()

def run(command, env={}):
    merged_env = os.environ
    merged_env.update(env)
    process = subprocess.Popen(command, stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT, shell=True,
                               env=merged_env)
    while True:
        line = process.stdout.readline()
        line = str(line, 'utf-8')[:-1]
        print(line)
        if line == '' and process.poll() != None:
            break
    if process.returncode != 0:
        raise Exception("Non zero return code: %d"%process.returncode)

parser = argparse.ArgumentParser(description='Example BIDS App entrypoint script.')
parser.add_argument('bids_dir', help='The directory with the input dataset '
                    'formatted according to the BIDS standard.')
parser.add_argument('output_dir', help='The directory where the output files '
                    'should be stored. If you are running group level analysis '
                    'this folder should be prepopulated with the results of the'
                    'participant level analysis.')
parser.add_argument('analysis_level', help='Level of the analysis that will be performed. '
                    'Multiple participant level analyses can be run independently '
                    '(in parallel) using the same output_dir.',
                    choices=['participant', 'group'])
parser.add_argument('--participant_label', help='The label(s) of the participant(s)'
                    'that should be analyzed. The label '
                   'corresponds to sub-<participant_label> from the BIDS spec '
                   '(so it does not include "sub-"). If this parameter is not '
                   'provided all subjects should be analyzed. Multiple '
                   'participants can be specified with a space separated list.',
                   nargs="+")
parser.add_argument('--session_label', help='The label of the session that should'
                    'be analyzed. The label '
                    'corresponds to ses-<session_label> from the BIDS spec '
                    '(so it does not include "ses-"). If this parameter is not '
                    'provided all sessions should be analyzed. Multiple '
                    'sessions can be specified with a space separated list.',
                    nargs="+")
parser.add_argument('--gpu', help='use cuda cores for eddy.',
                    choices=['yes', 'no'], default='no')

parser.add_argument('--skip_bids_validator', help='Whether or not to perform BIDS dataset validation',
                   action='store_true')
parser.add_argument('-v', '--version', action='version',
                    version='BIDS-App example version {}'.format(__version__))


args = parser.parse_args()

if not args.skip_bids_validator:
    run('bids-validator %s'%args.bids_dir)

subjects_to_analyze = []
# only for a subset of subjects
if args.participant_label:
    subjects_to_analyze = args.participant_label

# I believe I need something like the following to handle sessions.
# However, I do not understand the globbing AND this solution seems to fall short 
# in that it does not explicitly call the --session_label argument.
# There is an off chance that someone would have multiple DWI sessions

# A problem with all my scripts is that they cannot do this argument handling well
# That is, sometimes subject directories will have session subdirectories, and sometimes they will not.
# Perhaps this means I do have to abandon bash scripts

# for pt in args.participant_label:
    # file_paths+=glob(os.path.join(args.bids_dir,"sub-%s"%(pt),
    # "*","*.nii*"))+glob(os.path.join(args.bids_dir,"sub-%s"%(pt),
    # "*","*","*.nii*"))
    # else:
    # file_paths=glob(os.path.join(args.bids_dir,"*","*.nii*"))+\
    # glob(os.path.join(args.bids_dir,"*","*","*.nii*"))


# for all subjects
else:
    subject_dirs = glob(os.path.join(args.bids_dir, "sub-*"))
    subjects_to_analyze = [subject_dir.split("-")[-1] for subject_dir in subject_dirs]

# running participant level: 
# qc
if args.analysis_level == "participant":
    for subject_label in subjects_to_analyze:
        cmd = "qc.sh %s %s %s"%(subject_label,args.bids_dir,args.output_dir)
        print(cmd)
        run(cmd)

# running group level (this will be eddy_squad someday)
if args.analysis_level == "group":
    cmd = "group_qc.sh  %s"%(args.output_dir)
    print(cmd)
    run(cmd)
