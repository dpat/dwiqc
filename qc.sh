#!/bin/bash
#==============================================================================
: <<COMMENTBLOCK

Function:   BBRDWI
Purpose:    Register dwi image with Boundary Based Registration, optionally using fieldmaps
Input:      Depends on the following skull stripped images: mag, struct, dwi.
Output:     fmap_rads and registrations

COMMENTBLOCK

BBRDWI ()
{
	echo "------------------------------------------------"
	started=$(date "+%Y-%m-%d %T")
	echo "Starting BBRDWI UTC: ${started}"
	if [ -d ${bids_dir}/${subj}/fmap ]; then
		echo "fmap directory exists, so preparing fieldmaps"
		# Look up the EffectiveEchoSpacing for the dwi image in its json file
		ECHO_SPACE=$(cat ${bids_dir}/${subj}/dwi/${subj}_*dwi.json | grep -m 1 EffectiveEchoSpacing | sed 's/,//g' | awk '{print $2}')
		# Look up echo time for the first fieldmap
		ECHO_TIME1=$(cat ${output_dir}/${subj}/fmap/${subj}_magnitude1.json | grep -m 1 EchoTime | sed 's/,//g' | awk '{print $2}')
		# Look up echo time for the 2nd fieldmap
		ECHO_TIME2=$(cat ${output_dir}/${subj}/fmap/${subj}_phasediff.json | grep -m 1 EchoTime | sed 's/,//g' | awk '{print $2}')
		# Calculate the echo difference
		ECHO_DIFF=$(echo "scale=2; (${ECHO_TIME2} - ${ECHO_TIME1}) * 1000" | bc)
		# Get the phase encode direction from a file--because bids spec differs from FSL
		if [ -e ${bids_dir}/pe.txt ]; then
			PE=$(cat ${bids_dir}/pe.txt)
		else
			GetPE
			echo "PE=${PE}"
		fi

		if [ ! -e ${output_dir}/${subj}/fmap/fmap_rads.nii.gz ]; then
			echo "Prepare the Siemens fieldmap in radians, if we have an fmap directory"
			fsl_prepare_fieldmap SIEMENS ${output_dir}/${subj}/fmap/${subj}_phasediff.nii.gz ${output_dir}/${subj}/fmap/${subj}_magnitude1_brain.nii.gz ${output_dir}/${subj}/fmap/fmap_rads.nii.gz ${ECHO_DIFF}
		else
			echo "${output_dir}/${subj}/fmap/fmap_rads.nii.gz already exists, moving on"
		fi

		# If fmap exists, write b0 to T1w (str) registrations.
		if [ ! -e ${output_dir}/${subj}/reg/${subj}_b0_str.mat ]; then
			echo "running epi_reg with fieldmaps"
			epi_reg --fmap=${output_dir}/${subj}/fmap/fmap_rads.nii.gz \
			--fmapmag=${output_dir}/${subj}/fmap/${subj}_magnitude1.nii.gz \
			--fmapmagbrain=${output_dir}/${subj}/fmap/${subj}_magnitude1_brain.nii.gz \
			--pedir=${PE} --echospacing=${ECHO_SPACE} -v \
			--epi=${output_dir}/${subj}/dwi/nodif_brain.nii.gz \
			--t1=${bids_dir}/${subj}/anat/${subj}_T1w.nii.gz \
			--t1brain=${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat/${subj}_T1w_brain.nii.gz \
			--wmseg=${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat/${subj}_T1w_wmseg.nii.gz \
			--out=${output_dir}/${subj}/reg/${subj}_b0_str
		else
			echo "${output_dir}/${subj}/reg/${subj}_b0_str.mat already exists, moving on"
		fi
	fi

	# If no fmap exists, write b0 to T1w registrations.
	if [ ! -d ${bids_dir}/${subj}/fmap ] && [ ! -e ${output_dir}/${subj}/reg/${subj}_b0_str.mat ]; then
		echo "Running epi_reg without fieldmaps because no fmap directory exists"
		epi_reg --epi=${output_dir}/${subj}/dwi/nodif_brain.nii.gz --t1=${bids_dir}/${subj}/anat/${subj}_T1w.nii.gz --t1brain=${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat/${subj}_T1w_brain.nii.gz --out=${output_dir}/${subj}/reg/${subj}_b0_str -v
	fi

	if [ ! -e ${output_dir}/${subj}/reg/${subj}_str_b0.mat ]; then
		echo "flirt: invert b0_str.mat to create str_b0.mat if no fmap"
		convert_xfm -omat ${output_dir}/${subj}/reg/${subj}_str_b0.mat -inverse ${output_dir}/${subj}/reg/${subj}_b0_str.mat
	else
		echo "${output_dir}/${subj}/reg/${subj}_str_b0.mat already exists, moving on"
	fi

	if [ ! -e  ${output_dir}/${subj}/reg/${subj}_b0_mni_warp.nii.gz ]; then
		echo "fnirt: create a single warp b0_mni_warp"
		convertwarp -r ${FSLDIR}/data/standard/MNI152_T1_2mm -w ${output_dir}/${subj}/reg/${subj}_str_mni_warp.nii.gz -m ${output_dir}/${subj}/reg/${subj}_b0_str.mat -o ${output_dir}/${subj}/reg/${subj}_b0_mni_warp.nii.gz
	else
		echo "${output_dir}/${subj}/reg/${subj}_b0_mni_warp.nii.gz already exists, moving on"
	fi

	if [ ! -e  ${output_dir}/${subj}/reg/${subj}_mni_b0_warp.nii.gz ]; then
		echo "invert b0_mni_warp to create mni_b0_warp"
		invwarp -w ${output_dir}/${subj}/reg/${subj}_b0_mni_warp.nii.gz -o ${output_dir}/${subj}/reg/${subj}_mni_b0_warp.nii.gz -r ${output_dir}/${subj}/dwi/nodif_brain.nii.gz
	else
		echo "${output_dir}/${subj}/reg/${subj}_mni_b0_warp.nii.gz already exists, moving on"
	fi

	finished=$(date "+%Y-%m-%d %T")
	echo "BBRDWI has completed UTC: ${finished}"
}

#==============================================================================

: <<COMMENTBLOCK

Function:   CheckFilesQC
Purpose:    Determine whether the directories and files needed for processing are present.
            Create index.txt if it is missing.
            Exit gracefully, producing an error log, if there is a problem that will prevent
            dwiqc from running.
Input:      A BIDS directory with dwi and anat files at least
Output:     Information.  If there is a problem, then ${bids_dir}/${subj}_error.txt is generated,
            and subsequent processing is prevented.
COMMENTBLOCK

CheckFilesQC ()
{
  echo "========================================"
  echo "Checking that critical files exist for running dwiqc."
  echo "dwiqc will exit and produce error report if not."

  err_txt=${bids_dir}/${subj}_error.txt
  CheckDir ${bids_dir}/${subj}/ "subject dir exists" "subject dir missing"
  subj_dir_exist=${dir_exist}
  if [ "${subj_dir_exist}" = "True" ]; then
    ##################ANAT####################
    ##########################################
    echo ""
    echo "Checking anat files now"
    # Look for anat dir and related files
    CheckDir ${bids_dir}/${subj}/anat "anat dir exists" "anat dir missing"
    anat_dir_exist=${dir_exist}
    if [ "${anat_dir_exist}" = "True" ]; then
      CheckFiles ${bids_dir}/${subj}/anat/ "${subj}*T1*.nii.gz" "T1w exists" "No T1w"
      # Count the T1w images. BIPcan't handle multiple ones
      T1w_count=$(CountFiles ${bids_dir}/${subj}/anat/ "${subj}*T1*.nii.gz")
      #echo "T1w_count is ${T1w_count}"
      if [ ${T1w_count} -gt 1 ]; then
        T1w_count_fail_msg="There are multiple anat/T1w volumes. This is a problem for BIP."
        touch ${err_txt}
        if [ ! -e ${err_txt} ]; then
          touch ${err_txt}
          echo "===========================================" >> ${err_txt}
          echo ${subj} >> ${err_txt}
          echo "${T1w_count_fail_msg}" >> ${err_txt}
        fi
          echo "${T1w_count_fail_msg}"
          echo "${T1w_count_fail_msg}" >> ${err_txt}
      fi
      # Check to see if the intermediate ${subj}*T1w.anat dir exists
      # in the bids dir.  Remove it if it does because dwiqc will fail if it finds it.
      anat_subdir_exist=$(DirExist ${bids_dir}/${subj}/anat/${subj}*.anat)
      if [ "$anat_subdir_exist" = "True" ]; then
        rm -fr ${bids_dir}/${subj}/anat/${subj}*.anat
      fi
    fi

    ##################DWI####################
    ##########################################
    echo ""
    echo "Checking dwi files now"
    CheckDir ${bids_dir}/${subj}/dwi "dwi dir exists" "dwi dir missing"
    dwi_dir_exist=${dir_exist}
    if [ "${dwi_dir_exist}" = "True" ]; then
      CheckFiles ${bids_dir}/${subj}/dwi/ "${subj}_*dwi.nii.gz" "dwi image exists" "No dwi image"
      dwi_image_exist=$(FileExists "${bids_dir}/${subj}/dwi/" "${subj}_*dwi.nii.gz")
      if [ "$dwi_image_exist" = "0" ]; then
        DIM4=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim4 | awk '{print $2}')
        if [[ ${DIM4} -lt 6 ]]; then
          touch ${err_txt}
          echo "dwi has ${DIM4} volumes.  Something is wrong."; echo "dwi has ${DIM4} volumes.  Something is wrong." >> ${err_txt}
        else
          echo "The dwi has ${DIM4} volumes. Ensuring that index.txt exists."
          if [ ! -e ${bids_dir}/index*.txt ]; then
            touch ${bids_dir}/index.txt
          	numvols=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim4 | awk '{print $2}')
          	indx=" "
          	for ((i=1; i<=${numvols}; i+=1)); do indx="$indx 1"; done
          	echo $indx > ${bids_dir}/index.txt
          fi
        fi
        DIM3=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim3 | awk '{print $2}')
       # if there are fieldmaps, we presume we can run topup.
       # Check if there is not odd number of slices for topup and applytopup
       # if there is an odd number, discard the last slice of both A-P and P-A images
        # The %2 checks if DIM3 is even (divisible by 2).
        if [[ ${DIM3}%2 -eq 0 ]]; then
         echo "DWI image has even number of slices,${DIM3}. Good!"
        else
         DIM1=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim1 | awk '{print $2}')
         DIM2=$(fslinfo ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz | grep -m 1 dim2 | awk '{print $2}')
         # A math expression may be placed in double ()
         ((FINsl=${DIM3} - 1))
				 chmod u+w ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz
         fslroi ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz 0 ${DIM1} 0 ${DIM2} 0 ${FINsl}
				 chmod u+w ${bids_dir}/${subj}/fmap/${subj}_dir-*epi.nii.gz
         fslroi ${bids_dir}/${subj}/fmap/${subj}_dir-*epi.nii.gz ${bids_dir}/${subj}/fmap/${subj}_dir-*epi.nii.gz 0 ${DIM1} 0 ${DIM2} 0 ${FINsl}
        fi
      fi
    fi

    ##################FMAP####################
    ##########################################
	CheckDir ${bids_dir}/${subj}/fmap "fmap dir exists" "fmap dir missing"
    fmap_dir_exist=${dir_exist}
    if [ "${fmap_dir_exist}" = "True" ]; then
      echo ""
      echo "Checking fmap files now"
      CheckFiles ${bids_dir}/${subj}/fmap/ "${subj}_*magnitude1.nii.gz" "magnitude1 exists" "No magnitude1 image"
      CheckFiles ${bids_dir}/${subj}/fmap/ "${subj}_*magnitude2.nii.gz" "magnitude2 exists" "No magnitude2 image"
      CheckFiles ${bids_dir}/${subj}/fmap/ "${subj}_*phasediff.nii.gz" "phasediff exists" "No phasediff image"
      CheckFiles ${bids_dir}/${subj}/fmap/ "${subj}_dir-*epi.nii.gz" "ReversePE exists" "No ReversePE image"
    fi
  fi

  # TO DO: Add check for text files acqp* (or calculate these)
}

#==============================================================================
: <<COMMENTBLOCK

Function:   DWICorrect
Purpose:    Motion and distortion corrected DWI data (use eddy if we have
						reverse phase encode images). Otherwise, use eddy_correct.
Input:      Original 4D image
Output:     DWI corrected (ready for MkBed).
						topup, eddy or eddy_correct results.
NOTES: 			GPU code not working, removed
COMMENTBLOCK

DWICorrect ()
{
	echo "------------------------------------------------"
	started=$(date "+%Y-%m-%d %T")
	echo "starting DWICorrect UTC: ${started}"
	cp ${bids_dir}/${subj}/dwi/${subj}*.bvec ${output_dir}/${subj}/dwi/bvecs
	cp ${bids_dir}/${subj}/dwi/${subj}*.bval ${output_dir}/${subj}/dwi/bvals

	# If we do have the fmap directory
	if [ ! -e ${output_dir}/${subj}/dwi/data.nii.gz ]; then
	  im1=$(ls ${bids_dir}/${subj}/dwi/${subj}_*_dwi.nii.gz)
		im1_base=$(remove_ext ${im1})
		ind=$(ls ${bids_dir}/index*.txt)
		acqp=$(ls ${bids_dir}/acqp_*.txt)

		eddy --imain=${im1_base} --mask=${output_dir}/${subj}/dwi/nodif_brain_mask.nii.gz --index=${ind} --acqp=${acqp} --bvecs=${output_dir}/${subj}/dwi/bvecs --bvals=${output_dir}/${subj}/dwi/bvals  --out=${output_dir}/${subj}/dwi/data --topup=${output_dir}/${subj}/dwi/top_out --verbose
	else
		echo "eddy was already completed, moving on"
	fi

	finished=$(date "+%Y-%m-%d %T")
	echo "DWICorrect has completed UTC: ${finished}"
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Erode
Purpose:    erode a mask to make it smaller
Input:      magnitude1 image in ${bids_dir}/fmap
Output:     Eroded mask for magnitude1 image.

COMMENTBLOCK

Erode ()
{
  image=$1

  i=1
  while [ $i -le 2 ]; do
    echo "eroding ${i}"
    fslmaths ${image} -kernel 3D -ero ${image} -odt char
    let i+=1
  done
}

#==============================================================================

: <<COMMENTBLOCK
Function:   FindB0s
Purpose:    identify the locations of the B0 images from the bvals file
Caller:     PrepDWI and Top
Input:      The bvals file must exist in the subject's dwi directory
Output:     2 values representing the volume numbers of the first and
            last B0 image: B0_1 and B0_2

COMMENTBLOCK

FindB0s ()
{
  # Start the count at 0
  count=0
  # Declare an array to hold the positions of the B0 images
  declare -a B0_array=()
  # start the array index at 0
  i=0

  # Read the bval file to find the B0 volumes
  while read line; do
    for word in $line; do
      if [ "${word}" = "0" ]; then
        B0_array[$i]=${count}
        echo "B0_array[$i]=${count}"
        # increment the array index inside the if so
        # the index reflects the number of B0s rather
        # than the number of words examined
        let "i=i+1"
      fi
      # increment the count
      let "count=count+1"
    done
  done < ${bids_dir}/${subj}/dwi/${subj}*dwi.bval

  # Define the first and last elements of the array
  # using older bash syntax for arrays
  B0_1=${B0_array[@]:0:1}
  B0_2=${B0_array[${#B0_array[@]}-1]}
}

#==============================================================================

: <<COMMENTBLOCK

Function:   FSLAnatProc
Purpose:    Prep the T1w image
Input:      T1w image in ${bids_dir}/anat
Output:     T1w.anat directory

COMMENTBLOCK

FSLAnatProc ()
{
  echo "------------------------------------------------"
  started=$(date "+%Y-%m-%d %T")
  echo "Starting FSLAnatProc UTC: ${started}"

  if [ ! -d ${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat/${subj}_T1w.anat ]; then
    echo "Running fsl_anat_alt with optiBET"
    if [ ! -d ${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat ]; then
      mkdir -p ${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat
    fi

    # Define useful variables, given that we do not know the exact name of the T1w file
    anat_img=$(ls ${bids_dir}/${subj}/anat/${subj}*T1*.nii.gz)
    anat_img_base=$(basename ${anat_img})
    anat_img_base_stem=$(remove_ext ${anat_img_base})
		chmod u+w ${bids_dir}/${subj}/anat/
    chmod u+w ${anat_img}

    echo "Copy the original image to sourcedata for safe keeping"
    if [ ! -e ${bids_dir}/sourcedata/${subj}/anat/${subj}_T1w_pre_fsl_anat_proc.nii.gz ]; then
      cp ${anat_img} ${bids_dir}/sourcedata/${subj}/anat/${subj}_T1w_pre_fsl_anat_proc.nii.gz
    fi
    # Ensure orientation is correct
    fslreorient2std ${anat_img} ${anat_img}
    # If the lesion mask exists, then process it too.
    if [ -e ${bids_dir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz ]; then
      echo "There is a lesion mask to be used in processing."
      echo "Copy original lesion mask to sourcedata for safe keeping!!!!"
      echo "The new lesion-mask will be cropped."
      if [ ! -e ${bids_dir}/sourcedata/${subj}/anat/${subj}_label-lesion_roi.nii.gz ]; then
        cp ${bids_dir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz ${bids_dir}/sourcedata/${subj}/anat/
      fi
      fsl_anat_alt -i ${bids_dir}/${subj}/anat/${anat_img_base} -m ${bids_dir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz --noseg --nosubcortseg

      echo "Replace original lesion mask with cropped lesion mask"
      cp ${bids_dir}/${subj}/anat/${subj}*.anat/lesionmask.nii.gz ${bids_dir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz
    else
      fsl_anat_alt -i ${bids_dir}/${subj}/anat/${anat_img_base} --noseg --nosubcortseg
    fi

    echo "fsl_anat_alt has finished"
    echo "------------------------------------------------"

    echo "Replace original T1w image with cropped bias-corrected image with simple name"
    cp ${bids_dir}/${subj}/anat/${subj}*.anat/T1_biascorr.nii.gz ${bids_dir}/${subj}/anat/${subj}_T1w.nii.gz

    echo "Copy the brain_mask to derivatives/fsl_anat_proc"
    cp ${bids_dir}/${subj}/anat/*.anat/T1_biascorr_brain_mask.nii.gz ${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat/${subj}_T1w_mask.nii.gz

    echo "move the anat subdir to derivatives/fsl_anat_proc"
    mv ${bids_dir}/${subj}/anat/${subj}*.anat ${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat/
  fi

  finished=$(date "+%Y-%m-%d %T")
  echo "FSLAnatProc has completed UTC: ${finished}"
}
#==============================================================================


: <<COMMENTBLOCK
Function:   GetPE
Purpose:    Get the Phase Encode direction information for the primary DWI image.
            epi_reg requires a phase encode direction (--pedir) for the primary
            dwi image if you have fieldmaps. Unfortunately, the JSON files use i,j,k and
            FSL uses x,y,z to record this value. "PhaseEncodingDirection": "j-"
            in the json file corresponds to -y in FSL's terminology (-y=AP).
						If it exists, we can read ${bids_dir}/pe.txt to get this value (e.g., -y),
  					else this function returns the value to the caller.
Caller:			BBRDWI
Input:      The function will look for the json file in the bids dwi dir
Output:     A variable, PE containing the primary direction of the phase encode.
            This value will be in the xyz format that FSL expects rather than the ijk
            format stored in the json file.

COMMENTBLOCK

GetPE ()
{
	# epi_reg requires the phase encoding direction of the primary dwi image.
	# This is available in ijk coordinates in the json file but must be
	# translated into xyz coordinates for FSL
	  # Find the relevant value in the json file for the main DWI image
	PE_IJK=$(cat ${bids_dir}/${subj}/dwi/${subj}_*dwi.json | grep "PhaseEncodingDirection\"\:" | sed 's/,//g' | sed 's/"//g' | awk '{print $2}')

	# If we find the value, then translate it to xyz coordinates for epi_reg
	# x=i, y=j, z=k
	if [ ${PE_IJK} = "i" ]; then
	  PE_XYZ="x"
	  elif [ ${PE_IJK} = "i-" ]; then
	    PE_XYZ="-x"
	  elif [ ${PE_IJK} = "j" ]; then
	    PE_XYZ="y"
	  elif [ ${PE_IJK} = "j-" ]; then
	    PE_XYZ="-y"
	  elif [ ${PE_IJK} = "k" ]; then
	    PE_XYZ="z"
	  elif [ ${PE_IJK} = "k-" ]; then
	    PE_XYZ="-z"
	fi

	# Global variable PE returned from this function
	PE=${PE_XYZ}
}

#==============================================================================


: <<COMMENTBLOCK

Function:   MakeACQ
Purpose:    Create acqparams.txt.
            See https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/eddy/Faq
Output:
NOT IMPLEMENTED

COMMENTBLOCK

MakeACQ ()
{
	# To be used for acqp.txt and acqparams.txt:
	TOTAL_READOUT_TIME=$(cat ${bids_dir}/${subj}/dwi/${subj}_*dwi.json | grep -m 1 TotalReadoutTime | sed 's/,//g' | awk '{print $2}')
}


#==============================================================================

: <<COMMENTBLOCK

Function:   MakeDirs
Purpose:    Create subdirectories we will need
Output:     directory structure

COMMENTBLOCK

MakeDirs ()
{
	if [ ! -d ${bids_dir}/sourcedata/${subj}/anat ]; then
		mkdir -p ${bids_dir}/sourcedata/${subj}/anat
	fi

	if [ ! -d ${output_dir}/${subj} ]; then
  	mkdir -p ${output_dir}/${subj}/{dwi,reg,fmap}
	fi
}

#==============================================================================

: <<COMMENTBLOCK

Function:   PrepDWI
Purpose:    Create default B0 brain_mask by transforming T1w mask into B0 space.
            Precedes motion correction and bedpost
Input:      4D dwi image (or images, if you have reverse phase encode)
Output:     Default mask for each image. If available, fieldmaps and topup help
						create an undistorted image.
Notes:			I create temporary registrations. That is not necessary because we
						do not 	remask
COMMENTBLOCK

PrepDWI ()
{

  echo "------------------------------------------------"
  started=$(date "+%Y-%m-%d %T")
  echo "Starting PrepDWI UTC: ${started}"

  if [ ! -e ${output_dir}/${subj}/dwi/nodif_brain.nii.gz ]; then
    echo "------------------------------------------------"
    echo "Running DWI Brain Extraction"
  	echo "If there is a fmap dir, topup processing, be patient"
    mkdir -p ${output_dir}/${subj}/dwi
  	Top
  	if [ ! -e ${output_dir}/${subj}/dwi/nodif_brain_mask.nii.gz ]; then
      echo "create B0 brain mask"
      # Create temporary linear registration matrix of B0 to T1w
      flirt -in ${output_dir}/${subj}/dwi/nodif -ref ${bids_dir}/${subj}/anat/${subj}_T1w.nii.gz -omat ${output_dir}/${subj}/reg/${subj}_B02T1w_temp.mat -cost mutualinfo
      # Create temporary inverse matrix (T1w to B0)
      convert_xfm -omat ${output_dir}/${subj}/reg/${subj}_T1w2B0_temp.mat -inverse ${output_dir}/${subj}/reg/${subj}_B02T1w_temp.mat
      # Apply inverse matrix to T1w brain mask to create DWI brain mask
      flirt -in ${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat/${subj}_T1w_mask.nii.gz -applyxfm -init ${output_dir}/${subj}/reg/${subj}_T1w2B0_temp.mat -out ${output_dir}/${subj}/dwi/nodif_brain_mask -interp nearestneighbour -ref ${output_dir}/${subj}/dwi/nodif
  	fi
  else
  	echo "B0 brain mask was already created.  Moving on."
  fi

  finished=$(date "+%Y-%m-%d %T")
  echo "PrepDWI has completed UTC: ${finished}"
}

#==============================================================================

: <<COMMENTBLOCK

Function:   PrepMAG
Purpose:    Extract default mag brain_mask
Input:      Magnitude images must be available. Pick first magnitude image if multiple ones exist.
Output:     Eroded mask for MAG image, copy of magnitude and phase images into
            derivatives directory with expected names.
            This facilitates the use of prep later.

COMMENTBLOCK

PrepMAG ()
{
  for firstmag in ${bids_dir}/${subj}/fmap/${subj}*_magnitude1.nii.gz; do
    if [ -d ${bids_dir}/${subj}/fmap ] && [ ! -e ${output_dir}/${subj}/fmap/${subj}_magnitude1_brain_mask.nii.gz ]; then
      echo "------------------------------------------------"
  	  echo "Running registration and eroding, Magnitude1 Only"
      basemag=$(basename -s _magnitude1.nii.gz ${firstmag})
      cp ${bids_dir}/${subj}/fmap/${basemag}_magnitude1.nii.gz ${output_dir}/${subj}/fmap/${subj}_magnitude1.nii.gz
      cp ${bids_dir}/${subj}/fmap/${basemag}_magnitude1.json ${output_dir}/${subj}/fmap/${subj}_magnitude1.json
      cp ${bids_dir}/${subj}/fmap/${basemag}_phasediff.nii.gz ${output_dir}/${subj}/fmap/${subj}_phasediff.nii.gz
      cp ${bids_dir}/${subj}/fmap/${basemag}_phasediff.json ${output_dir}/${subj}/fmap/${subj}_phasediff.json
      # Create temporary linear registration matrix of magnitude to T1w
      flirt -in ${output_dir}/${subj}/fmap/${subj}_magnitude1.nii.gz -ref ${bids_dir}/${subj}/anat/${subj}_T1w.nii.gz -omat ${output_dir}/${subj}/reg/${subj}_Mag2T1w.mat -cost mutualinfo
      # Create inverse matrix (T1w to Mag)
      convert_xfm -omat ${output_dir}/${subj}/reg/${subj}_T1w2Mag.mat -inverse ${output_dir}/${subj}/reg/${subj}_Mag2T1w.mat
      # Apply inverse matrix to T1w brain mask to create MAG brain mask
      flirt -in ${bids_dir}/derivatives/fsl_anat_proc/${subj}/anat/${subj}_T1w_mask.nii.gz -applyxfm -init ${output_dir}/${subj}/reg/${subj}_T1w2Mag.mat -out ${output_dir}/${subj}/fmap/${subj}_magnitude1_brain_mask -interp nearestneighbour -ref ${output_dir}/${subj}/fmap/${subj}_magnitude1.nii.gz
      Erode ${output_dir}/${subj}/fmap/${subj}_magnitude1_brain_mask
      else
        echo "The mag image was already extracted.  Moving on."
    fi
  done
}
#==============================================================================

: <<COMMENTBLOCK

Function:   PrepT1w
Purpose:    segment
Input:      T1w images must be available in the anat dir
Output:     registered and segmented

Note the original .anat subdirectory has now been moved to the subject output dir:
We have a defaced, cropped, bias field corrected image, the linear and nonlinear registration, the corrected brain mask, and, if relevant, the lesion mask and inverse lesion masks.
#### TISSUE-TYPE SEGMENTATION
# required input: ${T1}_biascorr ${T1}_biascorr_brain ${T1}_biascorr_brain_mask
# output: ${T1}_biascorr ${T1}_biascorr_brain (modified) ${T1}_fast* (as normally output by fast) ${T1}_fast_bias (modified)


COMMENTBLOCK

PrepT1w ()
{
	echo "------------------------------------------------"
	started=$(date "+%Y-%m-%d %T")
	echo "Starting PrepT1w UTC: ${started}"
	if [ -d ${anat_outdir}/${subj}/anat/${subj}*.anat ]; then
		if [ ! -e ${anat_outdir}/${subj}/anat/${subj}_T1w_csfseg.nii.gz ] || [ ! -e ${anat_outdir}/${subj}/anat/${subj}_T1w_gmseg.nii.gz ] || [ ! -e ${anat_outdir}/${subj}/anat/${subj}_T1w_wmseg.nii.gz ]|| [ ! -e ${output_dir}/${subj}/reg/${subj}_str_mni_warp.nii.gz ] || [ ! -e ${output_dir}/${subj}/reg/${subj}_mni_str_warp.nii.gz ]; then

			if [ -e ${bids_dir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz ]; then
				echo "There is a lesion mask to be used in processing."
				echo "tissue segmentation only"
				# Run fsl_anat_alt
				fsl_anat_alt -d ${anat_outdir}/${subj}/anat/${subj}*.anat -o ${anat_outdir}/${subj}/anat/${subj}_T1w -m ${bids_dir}/${subj}/anat/${subj}_label-lesion_roi.nii.gz  --noreorient --nocrop --nobias --nobet --noreg --nononlinreg --nosubcortseg
			else
				fsl_anat_alt -d ${anat_outdir}/${subj}/anat/${subj}*.anat -o ${anat_outdir}/${subj}/anat/${subj}_T1w --noreorient --nocrop --nobias --nobet --noreg --nononlinreg --nosubcortseg
				# Copy the files we need and give them more transparent names. Do not use links, as those do not transfer correctly with some programs (e.g., Globus).  Better to use a little more space than not be able to transfer the files reliably.
			fi
			cd ${anat_outdir}/${subj}/anat/${subj}*.anat
			cp T1_fast_pve_0.nii.gz ${anat_outdir}/${subj}/anat/${subj}_T1w_csfseg.nii.gz
			cp T1_fast_pve_1.nii.gz ${anat_outdir}/${subj}/anat/${subj}_T1w_gmseg.nii.gz
			cp T1_fast_pve_2.nii.gz ${anat_outdir}/${subj}/anat/${subj}_T1w_wmseg.nii.gz
			cp ${anat_outdir}/${subj}/anat/${subj}*.anat/T1_to_MNI_nonlin_field.nii.gz ${output_dir}/${subj}/reg/${subj}_str_mni_warp.nii.gz
			cp ${anat_outdir}/${subj}/anat/${subj}*.anat/MNI_to_T1_nonlin_field.nii.gz ${output_dir}/${subj}/reg/${subj}_mni_str_warp.nii.gz
			echo "The structural has now been processed"
		fi
	else
		echo "The structural image was already processed.  Moving on."
	fi

	finished=$(date "+%Y-%m-%d %T")
	echo "PrepT1w has completed UTC: ${finished}"
}

#==============================================================================
: <<COMMENTBLOCK

Function:   Remask
Purpose:    Remask any images for which masks may have been edited by user.
						This is harmless if you have not made any changes.

COMMENTBLOCK

Remask ()
{
	echo "------------------------------------------------"
	echo "running Remask"

	# Remask T1w image to produce extracted brain
	fslmaths ${bids_dir}/${subj}/anat/${subj}_T1w.nii.gz -mul ${anat_outdir}/${subj}/anat/${subj}_T1w_mask.nii.gz ${anat_outdir}/${subj}/anat/${subj}_T1w_brain.nii.gz -odt short

	# cp improved T1w mask back to *.anat subdir for use in segmentation
	cp ${anat_outdir}/${subj}/anat/${subj}_T1w_mask.nii.gz ${anat_outdir}/${subj}/anat/${subj}*.anat/T1_biascorr_brain_mask.nii.gz
	# cp improved T1w brain back to *.anat subdir for use in segmentation
	cp ${anat_outdir}/${subj}/anat/${subj}_T1w_brain.nii.gz ${anat_outdir}/${subj}/anat/${subj}_T1w.anat/T1_biascorr_brain.nii.gz
	#
	# Remask nodif (B0) to produce extracted brain
	fslmaths ${output_dir}/${subj}/dwi/nodif.nii.gz -mul ${output_dir}/${subj}/dwi/nodif_brain_mask.nii.gz ${output_dir}/${subj}/dwi/nodif_brain.nii.gz

		# Remask magnitude image to produce extracted brain
		fslmaths ${output_dir}/${subj}/fmap/${subj}_magnitude1.nii.gz -mul ${output_dir}/${subj}/fmap/${subj}_magnitude1_brain_mask.nii.gz ${output_dir}/${subj}/fmap/${subj}_magnitude1_brain.nii.gz

	echo "Remask has completed"
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Top
Purpose:    prepare dti data, topup, applytop, create B0 file.
Caller: 		the PrepDWI function
Input:      The dwi image and the reverse phase encode fieldmap must exist.
            ${bids_dir}/acqparams.txt is also necessary.
Output:     nodif (suitable for bet2), topup parameters and unwarped B0s

COMMENTBLOCK

Top ()
{
  if [ ! -e ${output_dir}/${subj}/dwi/top_out_fieldcoef.nii.gz ]; then
     echo "------------------------------------------------"
     echo "Setting up for Topup and applytopup"
     # Find the B0 images in the sequence by using the bvals file.
     # Average the first and last B0 to create blip1.
     FindB0s
     # Default Siemens stretchy eyeballs, blip down -1 A-P
     # Get ${B0_1} and concatenate it with ${B0_2} to create blip1
     fslroi ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz ${output_dir}/${subj}/dwi/blip1_a ${B0_1} 1
     fslroi ${bids_dir}/${subj}/dwi/${subj}_*dwi.nii.gz ${output_dir}/${subj}/dwi/blip1_b ${B0_2} 1
     # Create blip1 by merging blip1_a and blip1_b
     fslmerge -t ${output_dir}/${subj}/dwi/blip1 ${output_dir}/${subj}/dwi/blip1_a ${output_dir}/${subj}/dwi/blip1_b
     # Remove blip_1a and blip_1b
     imrm ${output_dir}/${subj}/dwi/blip1_a
     imrm ${output_dir}/${subj}/dwi/blip1_b

     # Reverse phase encode +1, blip_up, squishy eyeballs, P-A
  	 # Here we assume the blip2 image consists of at least 2 B0 images
     fslroi ${bids_dir}/${subj}/fmap/${subj}_dir-*epi.nii.gz ${output_dir}/${subj}/fmap/blip2 0 2
     # acqparameters are -1 then +1, so blip_down, blip_up or A-P, P-A
     fslmerge -t ${output_dir}/${subj}/dwi/b0_combo ${output_dir}/${subj}/dwi/blip1 ${output_dir}/${subj}/fmap/blip2
     echo "Starting topup"
  	 # This generates the topup parameters for the unwarping
     topup --imain=${output_dir}/${subj}/dwi/b0_combo --datain=${bids_dir}/acqparams.txt --config=b02b0.cnf --out=${output_dir}/${subj}/dwi/top_out --fout=${output_dir}/${subj}/dwi/top_hz --verbose
  else
  	 echo "topup has already been run. Moving on."
  fi

  # Run applytopup simply to get some unwarped B0 images to skull strip
  # and feed to eddy later. The result is nodif.nii.gz
  if [ ! -e ${output_dir}/${subj}/dwi/b0_hifi.nii.gz ]; then
    echo "Starting applytopup on B0 images, new version"
    applytopup --imain=${output_dir}/${subj}/dwi/blip1,${output_dir}/${subj}/fmap/blip2 --inindex=1,3 --method=jac --datain=${bids_dir}/acqparams.txt --topup=${output_dir}/${subj}/dwi/top_out --out=${output_dir}/${subj}/dwi/b0_hifi --verbose
    echo "Running fslroi to separate one B0 from applytopup result"
    fslroi ${output_dir}/${subj}/dwi/b0_hifi ${output_dir}/${subj}/dwi/nodif 0 1
  else
  	echo "applytopup work is already done. Moving on."
  fi
}

#==============================================================================

: <<COMMENTBLOCK
Function:   UpdateBidsIgnore
Purpose:    If .bidsignore is not set to ignore *.txt files, then set it to
Caller:
Input:
Output:     A revised .bidsignore

COMMENTBLOCK

UpdateBidsIgnore ()
{
  # Tell bids validator to ignore txt files
  touch ${bids_dir}/.bidsignore
  # Use grep -q to test for the existence of the string txt in .bidsignore
  stringexists=$(cat ${bids_dir}/.bidsignore | grep -q txt; echo $?)
  # grep returns 1 if the string does not exist and 0 if it does exist
  #So, if the test=1, then add *.txt to .bidsignore
  if [ ${stringexists} -eq 1 ]; then
    # Add *.txt to the items in .bidsignore
    echo "*.txt" >> ${bids_dir}/.bidsignore
    echo ".bidignore now contains the following:"
    cat ${bids_dir}/.bidsignore
  fi
}


#==============================================================================

: <<COMMENTBLOCK

Function:   QC_participant
Purpose:    Run eddy quad
Input:      The results of running eddy.
Output:     A subdirectory ${output_dir}/${subj}/dwi/data.qc containing quality
            assessment for the participant level dwi image

COMMENTBLOCK

QC_participant ()
{
	echo "Running eddy_quad: See ${output_dir}/${subj}/dwi/data.qc for results"
	if [ ! -d ${output_dir}/${subj}/dwi/data.qc ]; then
		eddy_quad ${output_dir}/${subj}/dwi/data -idx ${bids_dir}/index*.txt -par ${bids_dir}/acqp_AP.txt -m ${output_dir}/${subj}/dwi/nodif_brain_mask -b ${output_dir}/${subj}/dwi/bvals
		if [ ! -e ${output_dir}/quad_folders.txt ]; then
			touch ${output_dir}/quad_folders.txt
		fi
		echo "${subj}/dwi/data.qc" >> ${output_dir}/quad_folders.txt
	else
		echo "${output_dir}/${subj}/dwi/data.qc already exists.  Moving on."
	fi

}

#==============================================================================

: <<COMMENTBLOCK

Function:   HelpMessage
Purpose:    Display help
Calls:      None

COMMENTBLOCK

HelpMessage ()
{
	echo
	echo "Assuming a bids compliant data directory,"
	echo "run qc.sh with 3 arguments, bids_dir, output_dir and subjectnum:"
	echo "e.g., If your subject is sub-001"
	echo "qc.sh 001 . ./derivatives"
	echo "you should be in your bids_dir"
	echo
	exit 1
}

#==============================================================================

: <<COMMENTBLOCK

Function:   Main
Purpose:    Preprocess and generate masks and segmentations
Input:      A compliant bids directory with dwi and structural data
            Optional fieldmaps and reverse phase encode data can be used
            if available.
Output:     T1w (cropped and reoriented), B0 (nodif) and magnitude masks.
            Registrations to standard space.
            Eddy and eddy_qc results.

COMMENTBLOCK

Main ()
{
   CheckFilesQC
  if [ -e ${bids_dir}/${subj}_error.txt ]; then
    echo "Critical files are missing or incorrect!"
    echo "dwiqc cannot be run"
    echo "See ${subj}_error.txt"
		echo "If you have corrected the errors, then remove ${subj}_error.txt and try running again."
  elif [ ! -e ${bids_dir}/${subj}_error.txt ]; then
    started_dwiqc=$(date "+%Y-%m-%d %T")
    echo "Congratulations! No error file generated, running dwiqc now."
    echo "Starting dwiqc UTC: ${started_dwiqc}"
    echo ""
    echo "dwiqc will create output directories under derivatives:"
    echo "fsl_dwi_proc and fsl_anat_proc."
    echo "fsl_dwi_proc: will contain processed dwi images, registrations,"
    echo "and if you have appropriate fieldmap files, fmap images."
    echo "fsl_anat_proc: will contain cropped and segmented T1w data."
    echo "Original T1w data will be stored in sourcedata."
    echo "=================================================================="
   	UpdateBidsIgnore
    MakeDirs
    FSLAnatProc
	  PrepDWI
	  PrepMAG
		Remask
		PrepT1w
		# Run motion and distoriton correction on dwi image: eddy or eddy_correct
		DWICorrect
		# epi_reg to do boundary based registration of DWI to structural
		BBRDWI
	# echo "quality control metrics calculating"
	  QC_participant
  echo "Finished DWIQC UTC: ${finished}"
	fi
}

#==============================================================================
: <<COMMENTBLOCK
See Main
COMMENTBLOCK

# We pass 3 arguments from run.py: the subjectnum, bids_dir and output_dir

source ${DWIQC_HOME}/dwiqc_functions.sh
# We pass 3 arguments from run.py: the subjectnum, bids_dir and output_dir

subjectnum=$1
subj=sub-${subjectnum}
bids_dir=$2
output_dir=$3
anat_outdir=${bids_dir}/derivatives/fsl_anat_proc
dwi_outdir=${bids_dir}/derivatives/fsl_dwi_proc

subjectnum=$1
subj=sub-${subjectnum}
bids_dir=$2
output_dir=$3
###################
echo "subject=${subj}"
echo "bids_dir = ${bids_dir}"
echo "output_dir = ${output_dir}"

if [ $# -lt 1 ]
    then
        HelpMessage
        exit 1
fi

Main ${subjectnum} ${bids_dir} ${output_dir}
#==============================================================================
