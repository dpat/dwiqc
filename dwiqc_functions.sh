#!/bin/bash

#==============================================================================
: <<COMMENTBLOCK

Function:   Binarize
Purpose:    Threshold a continuous image to make it a 1-0 binary mask instead
Input:     	2 arguments: input file, output file
Output:     1-0 mask of input image

COMMENTBLOCK

Binarize ()
{
  output=$1
  fslmaths ${output} -thr 0.5 -bin ${output}_bin -odt char
  imrm ${output}
}

#==============================================================================

: <<COMMENTBLOCK

Function:   CheckDir
Purpose:    Determine whether files needed for processing are present. Produce
            messages to standard output, and an error file if there are problems
            and processing should not proceed.
Input:      directory name, file name, success_msg, fail_msg
Output:     Information.  If there is a problem, then an error file is generated.
Caller:     CheckFilesSetup, CheckFilesPrep
Calls:      From bip_functions: calls DirExists and ultimately dir_exist.py

COMMENTBLOCK

CheckDir ()
{
  dir_name=$1
  dir_success_msg=$2
  dir_fail_msg=$3
  err_txt=${bids_dir}/${subj}_error.txt

  dir_exist=$(DirExist $dir_name)

  if [ "${dir_exist}" = "False" ]; then
    if [ ! -e ${err_txt} ]; then
      touch ${err_txt}
      echo "===========================================" >> ${err_txt}
      echo ${subj} >> ${err_txt}
    fi
    echo "${dir_fail_msg}"
    echo "${dir_fail_msg}" >> ${err_txt}
  else
    echo "${dir_success_msg}"
  fi
}


#==============================================================================

: <<COMMENTBLOCK

Function:   CheckFiles
Purpose:    Determine whether files needed for processing are present. Produce
            messages to standard output, and an error file if there are problems
            and processing should not proceed.
Input:      directory name, file name, success_msg, fail_msg
Output:     Information.  If there is a problem, then an error file is generated.
Caller:     CheckFilesSetup, CheckFilesPrep
Calls:      From bip_functions: calls FileExists and or CountFiles

COMMENTBLOCK

CheckFiles ()
{
  dir_name=$1
  file_name=$2
  success_msg=$3
  fail_msg=$4
  err_txt=${bids_dir}/${subj}_error.txt

  local exists=$(FileExists $dir_name $file_name)
  if [ "$exists" = "0" ]
  then
    echo "${success_msg}"
  else
    echo "${fail_msg}"
    if [ ! -e ${err_txt} ]; then
      touch ${err_txt}
      echo "===========================================" >> ${err_txt}
      echo "${subj}" >> ${err_txt}
    else
      echo "${fail_msg}"
    fi
    echo "${fail_msg}" >> ${err_txt}
  fi
}

#==============================================================================
: <<COMMENTBLOCK

Function:   CountFiles
Purpose:    To count how many files meet criterion by calling file_counter.py
Input:      Two arguments: directory and file pattern
Output:     A count of how many files meet the criterion
Caller:     CheckFiles calls this

COMMENTBLOCK

CountFiles ()
{
  local FILE_COUNT=$(file_counter.py $1 $2)
  echo $FILE_COUNT
}

#==============================================================================
: <<COMMENTBLOCK

Function:   DirExist
Purpose:    To determine whether a directory exists that meet criterion
Input:      directory pattern
Output:     True if the directory exists, else False
Caller:     CheckFiles calls this

COMMENTBLOCK

DirExist ()
{
  dir_exist=$(dir_exist.py $1)
  echo "${dir_exist}"
}

#==============================================================================
: <<COMMENTBLOCK

Function:   FileExists
Purpose:    To determine whether any files exist that meet criterion
Input:      Two arguments: directory and file pattern
Output:     0 if the test is successful and the file exists, else 1 if the file does not exist.
Caller:     CheckFiles calls this

COMMENTBLOCK

FileExists ()
{
  local cnt=$(CountFiles $1 $2)
  if [ "$cnt" -gt 0 ]; then    # some files exist
    echo 0
  else                    # no files exist
    echo 1
  fi
}
